#ifndef ULTRASONICSENSOR_H_
#define ULTRASONICSENSOR_H_
#include "Mbed.h"

#define USSADDR 0x05

#if defined __cplusplus
extern "C" {
#endif

class USS{
    private:
    MI2C *i2c;
    uint8_t rdata;
    public:
    USS(MI2C *i2c_);
    int com();
    uint8_t read();
};


#if defined __cplusplus
}
#endif

#endif