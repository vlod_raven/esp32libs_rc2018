#include "ultrasonicsensor.h"

USS::USS(MI2C *i2c_)
{
    i2c = i2c_;
}
int USS::com()
{
    return i2c -> read(USSADDR,&rdata,1);
}
uint8_t USS::read()
{
    return rdata;
}