#include "switch.h"

Switch::Switch(MI2C *i2c_)
{
    i2c = i2c_;
}

uint8_t Switch::read()
{
    return rdata;
}

int Switch::read(int port_)
{
    if((port_ < 0 ) | (port_ >= 8))
        return -1;
    else
        if((rdata & (0b1 << port_)) !=0)
            return 1;
        else
            return 0;
}

void Switch::com()
{
    i2c -> read(SWITCHADDR,&rdata,1);
}