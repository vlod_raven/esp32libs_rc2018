#ifndef SWITCH_H_
#define SWITCH_H_
#define SWITCHADDR 0x20
#include "Mbed.h"

class Switch
{
    private:
    MI2C *i2c;
    uint8_t rdata;

    public:
    Switch(MI2C *i2c_);
    uint8_t read();
    int read(int);
    void com();

};
#endif