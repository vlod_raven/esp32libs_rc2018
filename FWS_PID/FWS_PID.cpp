#include "FWS_PID.h"

FWS_PID::FWS_PID(PID_MD *md1_, PID_MD *md2_, PID_MD *md3_, PID_MD *md4_, MD_Position *smd_):
pid(0.1,0.01,0.001)
{
    md1 = md1_;
    md2 = md2_;
    md3 = md3_;
    md4 = md4_;
    smd = smd_;
}

void FWS_PID::move(float rps_)
{
    stat = MOVE;
    trps = rps_;
    md1 -> settarget(trps);
    md2 -> settarget(trps);
    md3 -> settarget(trps);
    md4 -> settarget(trps);
}
void FWS_PID::turn(float rps_)
{
    stat = TURN;
    trps = rps_;
    md1 -> settarget(trps);
    md2 -> settarget(trps);
    md3 -> settarget(-trps);
    md4 -> settarget(-trps);
}
void FWS_PID::moveuntil(float round_)
{
    stat = MOVEUNTIL;
    pid.setgain(MKP,MKI,MKD);
    pid.setoutlim(MAXMOVERPS, -MAXMOVERPS);
    pid.setinlim(1000,-1000);
    pid.reset();
    pid.settarget(round_);
    float rev1 = (md1 -> md.getrev());
    float rev2 = md2 -> md.getrev();
    tmp = (rev1 + rev2) / 2;
    pid.setproc(0);
    trps = pid.calc();
    md1 -> settarget(trps);
    md2 -> settarget(trps);
    md3 -> settarget(trps);
    md4 -> settarget(trps);
}
void FWS_PID::turnuntil(float deg_)
{
    stat = TURNUNTIL;
    pid.setgain(TKP,TKI,TKD);
    pid.setoutlim(MAXTURNRPS, -MAXTURNRPS);
    pid.setinlim(360,-360);
    pid.reset();
    pid.settarget(deg_);
    tmp = mpu.getyaw();
    pid.setproc(0);
    trps = pid.calc();
    md1 -> settarget(trps);
    md2 -> settarget(trps);
    md3 -> settarget(-trps);
    md4 -> settarget(-trps);
}
void FWS_PID::steer(float deg_)
{
    smd -> SetPosition(deg_);
}
FWS_PID::status FWS_PID::getstatus()
{
    return stat;
}
void FWS_PID::update()
{
    switch(stat)
    {
        case MOVEUNTIL:
        {
            float rev1 = md1 -> md.getrev();
            float rev2 = md2 -> md.getrev();
            pid.setproc(((rev1 + rev2)/2) - tmp);
            trps = pid.calc();
            md1 -> settarget(trps);
            md2 -> settarget(trps);
            md3 -> settarget(trps);
            md4 -> settarget(trps);
            break;
        }
        case TURNUNTIL:
        {
            pid.setproc(mpu.getyaw() - tmp);
            trps = pid.calc();
            md1 -> settarget(trps);
            md2 -> settarget(trps);
            md3 -> settarget(-trps);
            md4 -> settarget(-trps);
            break;
        }
        case IDLE:
        case MOVE:
        case TURN:
        case BRAKE:
            break;
    }
}

void FWS_PID::brake()
{
    stat = BRAKE;
    md1 -> brake();
    md2 -> brake();
    md3 -> brake();
    md4 -> brake();
}