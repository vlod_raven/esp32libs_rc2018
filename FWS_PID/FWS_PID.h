#ifndef FWS_PID_H_
#define FWS_PID_H_ 
#include "FWS_PID.h"
#include "simplePID_MD.h"
#include "mpu9250.h"
#include "MD_Position.h"

#define MAXMOVERPS 5
#define MAXTURNRPS 3

#define MKP kp
#define MKI ki
#define MKD kd
#define TKP 0.9
#define TKI 0.09
#define TKD 0.01

#if defined __cplusplus
extern "C" {
#endif

class FWS_PID
{
    private:
    PID_MD *md1;
    PID_MD *md2;
    PID_MD *md3;
    PID_MD *md4;
    MD_Position *smd;

    //LightSensor ls;
    enum status{
        IDLE = 0,
        MOVE,
        TURN,
        MOVEUNTIL,
        TURNUNTIL,
        BRAKE
    };
    status stat = IDLE;
    float trps;
    float tmp;
    float kp,ki,kd;

    public:
    PID pid;
    FWS_PID(PID_MD *md1_, PID_MD *md2_, PID_MD *md3_, PID_MD *md4_, MD_Position *smd_);
    void move(float rps);
    void turn(float rps);
    void moveuntil(float distance_);
    void turnuntil(float deg_);
    void steer(float deg_);
    status getstatus();
    void update();
    void brake();
    inline void setgain(float kp_, float ki_, float kd_)
    {
        kp = kp_;
        ki = ki_;
        kd = kd_;
    }

    inline float getproc()
    {
        return pid.getproc();
    }
};


#if defined __cplusplus
}
#endif

#endif