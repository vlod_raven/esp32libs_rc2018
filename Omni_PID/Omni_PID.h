#ifndef OMNI_PID_H_
#define OMNI_PID_H_ 
#include "Omni_PID.h"
#include "simplePID_MD.h"
#include "mpu9250.h"
#include "degrad.h"

#define MAXTURNRPS 3
#define MAXM2RPS 1

#define TKP 0.9
#define TKI 0.09
#define TKD 0.01
#define M2KP 0.1
#define M2KI 0.01
#define M2KD 0.001

#if defined __cplusplus
extern "C" {
#endif

//Positive value turn direction is counter clockwise

class Omni_PID
{
    private:
    PID_MD *md1;
    PID_MD *md2;
    PID_MD *md3;
    PID_MD *md4;
    enum status{
        IDLE = 0,
        MOVE,
        MOVE2,
        TURN,
        MOVEUNTIL,
        TURNUNTIL,
        BRAKE
    };
    status stat = IDLE;
    float tmp;
    float kp,ki,kd;
    float m2rps,sin,cos;

    public:
    enum OmniDirection_t
    {
        FRONT = 0,
        BACK,
        RIGHT,
        LEFT,
        RIGHTFRONT,
        LEFTFRONT,
        RIGHTBACK,
        LEFTBACK,
    };
    PID pid;
    Omni_PID(PID_MD *md1_, PID_MD *md2_, PID_MD *md3_, PID_MD *md4_);
    void move(float rps_, OmniDirection_t dir_);
    void move1(float rps_, float dir_);
    void move2(float rps_, float dir_);
    void turn(float rps_);
    //void moveuntil(float distance_);
    void turnuntil(float deg_);
    status getstatus();
    void update();
    void brake();
    inline int turnprog()
    {
        if(stat == TURNUNTIL)
        {
            float temp = pid.gettarget() - (mpu.getyaw() - tmp);
            if(temp < 0.5 && temp > -0.5)
                return 1;
            return 0;
        }
        else
        return -1;
    }
    inline void setgain(float kp_, float ki_, float kd_)
    {
        kp = kp_;
        ki = ki_;
        kd = kd_;
    }

    inline float getproc()
    {
        return pid.getproc();
    }
    
    inline float gettrps1()
    {
        return md1 -> gettarget();
    }
    inline float gettrps2()
    {
        return md2 -> gettarget();
    }
    inline float gettrps3()
    {
        return md3 -> gettarget();
    }
    inline float gettrps4()
    {
        return md4 -> gettarget();
    }
};


#if defined __cplusplus
}
#endif

#endif