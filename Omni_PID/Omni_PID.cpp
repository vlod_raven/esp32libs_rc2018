#include "Omni_PID.h"

Omni_PID::Omni_PID(PID_MD *md1_, PID_MD *md2_, PID_MD *md3_, PID_MD *md4_):
pid(0.1,0.01,0.001)
{
    md1 = md1_;
    md2 = md2_;
    md3 = md3_;
    md4 = md4_;
    brake();
}

void Omni_PID::move(float rps_, OmniDirection_t dir_)
{
    stat = MOVE;
    switch(dir_)
    {
        case FRONT:
            md1 -> settarget(rps_);
            md2 -> settarget(-rps_);
            md3 -> settarget(rps_);
            md4 -> settarget(-rps_);
            break;
        case BACK:
            md1 -> settarget(-rps_);
            md2 -> settarget(rps_);
            md3 -> settarget(-rps_);
            md4 -> settarget(rps_);
            break;
        case RIGHT:
            md1 -> settarget(-rps_);
            md2 -> settarget(-rps_);
            md3 -> settarget(rps_);
            md4 -> settarget(rps_);
            break;
        case LEFT:
            md1 -> settarget(rps_);
            md2 -> settarget(rps_);
            md3 -> settarget(-rps_);
            md4 -> settarget(-rps_);
            break;
    }
}

void Omni_PID::move1(float rps_, float deg_)
{
    stat = MOVE;
    deg_ -= 45.0;
    float sin = sinf(deg2rad(deg_));
    float cos = cosf(deg2rad(deg_));
    md1 -> settarget(rps_ * sin);
    md2 -> settarget(-rps_ * cos);
    md3 -> settarget(rps_ * cos);
    md4 -> settarget(-rps_ * sin);
}
void Omni_PID::move2(float rps_, float deg_)
{
    stat = MOVE2;
    m2rps = rps_;
    pid.setgain(M2KP,M2KI,M2KD);
    pid.setoutlim(MAXM2RPS, -MAXM2RPS);
    pid.setinlim(360,-360);
    pid.reset();
    pid.settarget(0);
    tmp = mpu.getyaw();
    pid.setproc(0);
    float rps = pid.calc();

    deg_ -= 45.0;
    sin = sinf(deg2rad(deg_));
    cos = cosf(deg2rad(deg_));
    md1 -> settarget((m2rps + rps) * sin);
    md2 -> settarget((-m2rps + rps) * cos);
    md3 -> settarget((m2rps + rps) * cos);
    md4 -> settarget((-m2rps + rps) * sin);
}

void Omni_PID::turn(float rps_)
{
    stat = TURN;
    md1 -> settarget(rps_);
    md2 -> settarget(rps_);
    md3 -> settarget(rps_);
    md4 -> settarget(rps_);
}
/*
void Omni_PID::moveuntil(float round_)
{
    stat = MOVEUNTIL;
    pid.setgain(MKP,MKI,MKD);
    pid.setoutlim(MAXMOVERPS, -MAXMOVERPS);
    pid.setinlim(1000,-1000);
    pid.reset();
    pid.settarget(round_);
    float rev1 = (md1 -> md.getrev());
    float rev2 = md2 -> md.getrev();
    tmp = (rev1 + rev2) / 2;
    pid.setproc(0);
    trps1 = pid.calc();
    trps2 = trps1;
    md1 -> settarget(trps1);
    md2 -> settarget(trps2);
}
*/
void Omni_PID::turnuntil(float deg_)
{
    stat = TURNUNTIL;
    pid.setgain(TKP,TKI,TKD);
    pid.setoutlim(MAXTURNRPS, -MAXTURNRPS);
    pid.setinlim(360,-360);
    pid.reset();
    pid.settarget(deg_);
    tmp = mpu.getyaw();
    pid.setproc(0);
    float rps = pid.calc();
    md1 -> settarget(rps);
    md2 -> settarget(rps);
    md3 -> settarget(rps);
    md4 -> settarget(rps);
}
Omni_PID::status Omni_PID::getstatus()
{
    return stat;
}
void Omni_PID::update()
{
    switch(stat)
    {
        case MOVEUNTIL: break;
        case TURNUNTIL:
        {
            pid.setproc(mpu.getyaw() - tmp);
            float rps = pid.calc();
            md1 -> settarget(rps);
            md2 -> settarget(rps);
            md3 -> settarget(rps);
            md4 -> settarget(rps);
            break;
        }
        case MOVE2:
        {
            pid.setproc(mpu.getyaw() - tmp);
            float rps = pid.calc();
            md1 -> settarget((m2rps + rps) * sin);
            md2 -> settarget((-m2rps + rps) * cos);
            md3 -> settarget((m2rps + rps) * cos);
            md4 -> settarget((-m2rps + rps) * sin);
            break;
        }
        case IDLE:
        case MOVE:
        case TURN:
        case BRAKE:
            break;
    }
}

void Omni_PID::brake()
{
    stat = BRAKE;
    md1 -> brake();
    md2 -> brake();
    md3 -> brake();
    md4 -> brake();
}