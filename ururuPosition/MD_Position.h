#ifndef __MD_POSITION_H__
#define __MD_POSITION_H__
#include "Mbed.h"
#include "ururudef.h"
#include <Arduino.h>

class MD_Position{
    private:
    uint8_t data;
    public:
    void init(MSPI *spi_);
    MD_Position(MSPI *spi_);
    void SetPosition(float position);
    void operator = (float position);
    void ZeroAdjustment();
    MSPI *spi;
    void send();
};
#endif