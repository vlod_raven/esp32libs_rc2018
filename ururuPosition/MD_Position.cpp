#include "MD_Position.h"

void MD_Position::init(MSPI *spi_){
    spi = spi_;
    data = 0x80;
    //ZeroAdjustment();
}

MD_Position::MD_Position(MSPI *spi_){
    init(spi_);
}

void MD_Position::send(){
    {
        spi->write(230);
        wait(0.001);    
        spi->write(data);
        wait(0.001);
        spi->write(data);
        wait(0.001);
    }
}

void MD_Position::SetPosition(float position){
    if(position > 180.0)
        position = 180.0;
    if(position < 0.00)
        position = 0.00;
    data = (uint8_t(position / 180.0 * 200.0 ) + 0x1c);
}

void MD_Position::operator = (float position){
    SetPosition(position);
}

void MD_Position::ZeroAdjustment(){
    data = (0x80);
}