#include "TW_PID.h"

TW_PID::TW_PID(PID_MD *md1_, PID_MD *md2_):
pid(0.1,0.01,0.001)
{
    md1 = md1_;
    md2 = md2_;
    brake();
}

void TW_PID::move(float rps_)
{
    stat = MOVE;
    trps1 = rps_;
    trps2 = rps_;
    md1 -> settarget(trps1);
    md2 -> settarget(trps2);
}
void TW_PID::move(float rps1_, float rps2_)
{
    stat = MOVE;
    trps1 = rps1_;
    trps2 = rps2_;
    md1 -> settarget(trps1);
    md2 -> settarget(trps2);
}
void TW_PID::turn(float rps_)
{
    stat = TURN;
    trps1 = rps_;
    trps2 = rps_;
    md1 -> settarget(trps1);
    md2 -> settarget(-trps2);
}
void TW_PID::moveuntil(float round_)
{
    stat = MOVEUNTIL;
    pid.setgain(MKP,MKI,MKD);
    pid.setoutlim(MAXMOVERPS, -MAXMOVERPS);
    pid.setinlim(1000,-1000);
    pid.reset();
    pid.settarget(round_);
    float rev1 = (md1 -> md.getrev());
    float rev2 = md2 -> md.getrev();
    tmp = (rev1 + rev2) / 2;
    pid.setproc(0);
    trps1 = pid.calc();
    trps2 = trps1;
    md1 -> settarget(trps1);
    md2 -> settarget(trps2);
}
void TW_PID::turnuntil(float deg_)
{
    stat = TURNUNTIL;
    pid.setgain(TKP,TKI,TKD);
    pid.setoutlim(MAXTURNRPS, -MAXTURNRPS);
    pid.setinlim(360,-360);
    pid.reset();
    pid.settarget(deg_);
    tmp = mpu.getyaw();
    pid.setproc(0);
    trps1 = pid.calc();
    trps2 = trps1;
    md1 -> settarget(trps1);
    md2 -> settarget(-trps2);
}
TW_PID::status TW_PID::getstatus()
{
    return stat;
}
void TW_PID::update()
{
    switch(stat)
    {
        case MOVEUNTIL:
        {
            float rev1 = md1 -> md.getrev();
            float rev2 = md2 -> md.getrev();
            pid.setproc(((rev1 + rev2)/2) - tmp);
            trps1 = pid.calc();
            trps2 = trps1;
            md1 -> settarget(trps1);
            md2 -> settarget(trps2);
            break;
        }
        case TURNUNTIL:
        {
            pid.setproc(mpu.getyaw() - tmp);
            trps1 = pid.calc();
            trps2 = trps1;
            md1 -> settarget(trps1);
            md2 -> settarget(-trps2);
            break;
        }
        case IDLE:
        case MOVE:
        case TURN:
        case BRAKE:
            break;
    }
}

void TW_PID::brake()
{
    stat = BRAKE;
    md1 -> brake();
    md2 -> brake();
}