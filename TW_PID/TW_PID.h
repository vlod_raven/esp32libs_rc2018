#ifndef TW_PID_H_
#define TW_PID_H_ 
#include "TW_PID.h"
#include "simplePID_MD.h"
#include "mpu9250.h"

#define MAXMOVERPS 5
#define MAXTURNRPS 2

#define MKP kp
#define MKI ki
#define MKD kd
#define TKP 0.9
#define TKI 0.09
#define TKD 0.01

#if defined __cplusplus
extern "C" {
#endif

class TW_PID
{
    private:
    PID_MD *md1;
    PID_MD *md2;
    //LightSensor ls;
    enum status{
        IDLE = 0,
        MOVE,
        TURN,
        MOVEUNTIL,
        TURNUNTIL,
        BRAKE
    };
    status stat = IDLE;
    float trps1, trps2;
    float tmp;
    float kp,ki,kd;

    public:
    PID pid;
    TW_PID(PID_MD *md1_, PID_MD *md2_);
    void move(float rps_);
    void move(float rps1_, float rps2_);
    void turn(float rps_);
    void moveuntil(float distance_);
    void turnuntil(float deg_);
    status getstatus();
    void update();
    void brake();
    inline int turnprog()
    {
        if(stat == TURNUNTIL)
        {
            float temp = pid.gettarget() - (mpu.getyaw() - tmp);
            if(temp < 0.5 && temp > -0.5)
                return 1;
            return 0;
        }
        else
        return -1;
    }
    inline void setgain(float kp_, float ki_, float kd_)
    {
        kp = kp_;
        ki = ki_;
        kd = kd_;
    }

    inline float getproc()
    {
        return pid.getproc();
    }
    
    inline float gettrps1()
    {
        return trps1;
    }
    inline float gettrps2()
    {
        return trps2;
    }
};


#if defined __cplusplus
}
#endif

#endif