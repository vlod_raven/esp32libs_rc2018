#include "ururumother.h"
#include "ururudef.h"
#include "Mbed.h"

DigitalOut LED1(UM_LED1_PIN);
DigitalOut LED2(UM_LED2_PIN);
DigitalOut LED3(UM_LED3_PIN);
MSPI spi(&SPI);
MI2C i2c(&Wire);

#if (UM_OTAE == 1)
TaskHandle_t wificon = 0, wifiwd = 0;
float interval = 10;

void wifiwatchdog(void *arg);
void wificonnect(void *arg) //接続を試みる
{
    while(1)
    {
        WiFi.begin(UM_SSID, UM_WIFIPASS);
        if(WiFi.waitForConnectResult() != WL_CONNECTED) //接続失敗
        {
            UM_DEBUGSERIAL.printf("Failed to WiFi connecting");
            LED3 = 1;
            wait(0.5);
            LED3 = 0;
            wait((interval-0.5));
        }
        else //接続成功
        {
            UM_DEBUGSERIAL.println("Succeed to Wifi connecting");
            ArduinoOTA.begin();
            UM_DEBUGSERIAL.print("IP: ");
            UM_DEBUGSERIAL.println(WiFi.localIP());
            UM_DEBUGSERIAL.print("MAC: ");
            UM_DEBUGSERIAL.println(WiFi.macAddress());
            if(wifiwd == 0) //初回だったら
                xTaskCreatePinnedToCore(wifiwatchdog,"wifiwatchdog",4096,NULL,2,&wifiwd,1);
            else
                vTaskResume(wifiwd);
            vTaskSuspend(wificon);
            wait(1);
        }
    }
}

void wifiwatchdog(void *arg)    //接続状況を確かめる
{
    while(1)
    {
        if(WiFi.isConnected() != 1) //接続されていなかったら
        {
            UM_DEBUGSERIAL.println("Disconnected from Wifi");
            vTaskResume(wificon);
            vTaskSuspend(wifiwd);
        }
        LED3 = 0;
        wait(0.5);
        LED3 = 1;
        wait((interval - 0.5));
    }
}

void led3ota(void *arg)
{
    while(1)
    {
        LED3 = 0;
        wait(0.05);
        LED3 = 1;
        wait(0.05);
    }
}

void UMinterval(float sec_)
{
    if(sec_ < 5)
        interval = 5.0;
    else
        interval = sec_;
}
#endif

void led2blink(void *arg)
{
    while(1)
    {
        LED2 = 0;
        //wait(led2off);
        wait(1);
        LED2 = 1;
        //wait(led2on);
        wait(1);
    }
}

fptr_vv_t UMtask_i2c_init = NULL;
fptr_vv_t UMtask_spi_init = NULL;
fptr_vv_t UMtask_i2c_while = [](){};
fptr_vv_t UMtask_spi_while = [](){};

void UMtask_i2c(void *arg)
{
    Wire.begin(UM_SDA,UM_SCL,100000);
    if(UMtask_i2c_init != NULL)
        UMtask_i2c_init();
    portTickType lt = xTaskGetTickCount();
    while(1)
    {
        vTaskDelayUntil(&lt, 10/portTICK_RATE_MS);
        UMtask_i2c_while();
    }
}

void UMtask_spi(void *arg)
{
    SPI.begin(UM_SCLK,UM_MISO,UM_MOSI,-1);
    if(UMtask_i2c_init != NULL)
        UMtask_spi_init();
    portTickType lt = xTaskGetTickCount();
    while(1)
    {
        vTaskDelayUntil(&lt, 10/portTICK_RATE_MS);
        UMtask_spi_while();
    }
}

void UMinit()
{
    Serial.begin(115200);
    #if (UM_OTAE == 1)
    WiFi.mode(WIFI_STA);
    xTaskCreatePinnedToCore(wificonnect,"wificonnect",4096,NULL,2,&wificon,1); //接続を試みる
    // Port defaults to 3232                            // ArduinoOTA.setPort(3232);
    // Hostname defaults to esp3232-[MAC]               // ArduinoOTA.setHostname("myesp32");
    // No authentication by default                     // ArduinoOTA.setPassword("admin");
    // Password can be set with it's md5 value as well  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
    // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");
    ArduinoOTA.onStart([]() {
        String type;
        if (ArduinoOTA.getCommand() == U_FLASH)
            type = "sketch";
        else // U_SPIFFS
            type = "filesystem";
        // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
        UM_DEBUGSERIAL.println("Start updating " + type);
        vTaskDelete(wificon);
        vTaskDelete(wifiwd);
        xTaskCreatePinnedToCore(led3ota,"led3ota",1024,NULL,1,NULL,1);
    });
    ArduinoOTA.onEnd([]() {
        UM_DEBUGSERIAL.println("\nEnd");
    });
    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
        UM_DEBUGSERIAL.printf("Progress: %u%%\r", (progress / (total / 100)));
    });
    ArduinoOTA.onError([](ota_error_t error) {
        UM_DEBUGSERIAL.printf("Error[%u]: ", error);
        if (error == OTA_AUTH_ERROR)         UM_DEBUGSERIAL.println("Auth Failed");
        else if (error == OTA_BEGIN_ERROR)   UM_DEBUGSERIAL.println("Begin Failed");
        else if (error == OTA_CONNECT_ERROR) UM_DEBUGSERIAL.println("Connect Failed");
        else if (error == OTA_RECEIVE_ERROR) UM_DEBUGSERIAL.println("Receive Failed");
        else if (error == OTA_END_ERROR)     UM_DEBUGSERIAL.println("End Failed");
    });
    #endif

    #if (UM_MODE == UM_PS3)
    ps3_init();
    #endif

    xTaskCreatePinnedToCore(led2blink,"UMblink2",1024,NULL,1,NULL,1);
    xTaskCreatePinnedToCore(UMtask_i2c,"UMi2c",8096,NULL,5,NULL,0);
    xTaskCreatePinnedToCore(UMtask_spi,"UMspi",8096,NULL,4,NULL,1);

    #if (UM_MODE == UM_BTS)
    BTSerial.begin(UM_BTSNAME);
    #endif

    #if (UM_DEBUGLVL >= 1)
    UM_DEBUGSERIAL.printf("UM: Finish init\n");
    #endif

    //read emergency switch
    pinMode(UM_ESW, INPUT);
}