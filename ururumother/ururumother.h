#ifndef URURUMOTHER_H_
#define URURUMOTHER_H_
#include "ururudef.h"
#include <Arduino.h>
#include "Mbed.h"

#if (UM_OTAE == 1)
#include <ArduinoOTA.h>
#endif

#if (UM_MODE == UM_BTS)
#include "BTSerial.h"
#endif

#if (UM_MODE == UM_PS3)
#include "PS3.h"
#endif

#if defined __cplusplus
extern "C" {
#endif

//使用可能なクラスのオブジェクト
extern DigitalOut LED1;
extern DigitalOut LED2;
extern DigitalOut LED3;
extern MSPI spi;
extern MI2C i2c;

//I2CとSPIの関数ポインタ
//typedef void (*fptr_vv_t)();
extern fptr_vv_t UMtask_i2c_init;
extern fptr_vv_t UMtask_spi_init;
extern fptr_vv_t UMtask_i2c_while;
extern fptr_vv_t UMtask_spi_while;

void UMinit();  //setup()で１回実行

inline int readesw()
{
    return digitalRead(UM_ESW);
}

#if (UM_OTAE == 1)
void UMinterval(float sec_);    //WiFiの接続試行の間隔
#endif

#if defined __cplusplus
}
#endif
#endif