#ifndef BTSERIAL_H_
#define BTSERIAL_H_
#include <BluetoothSerial.h>

#if defined __cplusplus
extern "C" {
#endif

extern BluetoothSerial BTSerial;

#if defined __cplusplus
}
#endif

#endif