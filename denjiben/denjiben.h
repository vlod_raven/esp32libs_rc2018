#ifndef DENJIBEN_H_
#define DENJIBEN_H_

#include "ururudef.h"
#include "ururumother.h"
#include "Mbed.h"

#define DENJIADDR 0x21
#define OPENDURATION 0.3

#if defined __cplusplus
extern "C" {
#endif

class denjiben
{
    public:
    MI2C *i2c;
    uint8_t ports;
    unsigned long time[8]={};
    public:
    denjiben(MI2C *i2c_);
    void settime(int port_,unsigned long ms_);
    void set(uint8_t ports_);
    void set(int port_, int bit_);
    void com();
    void update();
    inline void reset()
    {
        ports = 0b0;
        for(int i = 0; i < 8; i++)
            time[i] = 0;
    }
};

#if defined __cplusplus
}
#endif

#endif