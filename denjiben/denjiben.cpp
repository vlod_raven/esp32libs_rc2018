#include "denjiben.h"

denjiben::denjiben(MI2C *i2c_)
{
    i2c = i2c_;
    reset();
}

void denjiben::settime(int port_,unsigned long ms_)
{
    if((port_ < 0) | (port_ > 7))
        return;
    time[port_] = millis() + ms_;
    ports |= (0b1 << port_);
}
void denjiben::set(uint8_t ports_)
{
    reset();
    ports = ports_;
}

void denjiben::set(int port_, int bit_)
{
    if(bit_ == 0)
        ports &= ~((uint8_t)(0b1 << port_));
    else
        ports |= ((uint8_t)(0b1 << port_));
}
void denjiben::com()
{
    i2c -> write(DENJIADDR, &ports, 1);
}
void denjiben::update()
{
    unsigned long nowtime = millis();
    for(int i = 0; i < 8; i++)
    {
        if((ports & (0b1 << i)) != 0)
        {
            if(time[i] != 0 && time[i] <= nowtime)
            {
                time[i] = 0;
                switch(i)
                {
                    case 0:
                        ports &= 0b11111110;
                        break;
                    case 1:
                        ports &= 0b11111101;
                        break;
                    case 2:
                        ports &= 0b11111011;
                        break;
                    case 3:
                        ports &= 0b11110111;
                        break;
                    case 4:
                        ports &= 0b11101111;
                        break;
                    case 5:
                        ports &= 0b11011111;
                        break;
                    case 6:
                        ports &= 0b10111111;
                        break;
                    case 7:
                        ports &= 0b01111111;
                        break;
                }
            }
        }
    }
}