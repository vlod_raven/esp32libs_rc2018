#include <Lightsensor.h>

Lightsensor::Lightsensor(MI2C *i2c_, uint8_t addr_)
{
    i2c = i2c_;
    addr = addr_;
    mode = 0b00010101;
    rev = 0;
    thres[0] = LSTHRESHOLD;
    thres[1] = LSTHRESHOLD;
    thres[2] = LSTHRESHOLD;
}

int Lightsensor::com()
{
    i2c -> write(addr,&mode,1);
    return i2c -> read(addr,rdata,3);
}

uint8_t Lightsensor::read(int num_)
{
    uint8_t tmp = num_;
    if(rev)
        tmp = 2 - tmp;
    switch(tmp)
    {
        case 0:
        case 1:
        case 2:
            return rdata[tmp];
            break;
    }
    return 0;
}

int Lightsensor::judge(lsposition_t posi_)
{
    if(read(posi_) > thres[posi_])
        return 1;
    return 0;
}

void Lightsensor::reverse(uint8_t rev_)
{
    rev = rev_;
}

void Lightsensor::setthresh(lsposition_t posi_, uint8_t thres_)
{
    uint8_t tmp = posi_;
    if(rev)
        tmp = 2 - tmp;
    switch(tmp)
    {
        case 0:
        case 1:
        case 2:
            thres[tmp] = thres_;
    }
}