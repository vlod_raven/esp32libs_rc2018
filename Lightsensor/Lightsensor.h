#ifndef LIGHTSENSOR_H_
#define LIGHTSENSOR_H_
#include "Mbed.h"

#define LSADDRF 0x0f
#define LSADDRB 0x0b

#define LSTHRESHOLD 43

#if defined __cplusplus
extern "C" {
#endif

enum lsposition_t{
    right = 0,
    middle,
    left
};

/*
enum lscolor_t{
    white = 0,
    blue,
    red
};
*/

class Lightsensor{
    private:
    MI2C *i2c;
    uint8_t rdata[3];
    uint8_t thres[3];
    uint8_t addr;
    uint8_t mode;
    uint8_t rev;
    public:
    Lightsensor(MI2C *i2c_, uint8_t addr_);
    int com();
    uint8_t read(int num_);
    int judge(lsposition_t posi_);
    void reverse(uint8_t rev_);
    void setthresh(lsposition_t posi_, uint8_t thres_);
};

#if defined __cplusplus
}
#endif

#endif