#ifndef DEGRAD_H_
#define DEGRAD_H_

#ifndef M_PI
#define M_PI 3.14159265358979
#endif
#define VAL1 0.0174532925199f // PI/180
#define VAL2 57.295779513082f // 180/PI

inline float deg2rad(float deg)
{
    return (deg*VAL1);
}
inline float rad2deg(float rad)
{
    return (rad*VAL2);
}

#endif