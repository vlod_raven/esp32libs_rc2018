#ifndef LUMPDO_H_
#define LUMPDO_H_

/*
ptr_: it is array of pointer to object of any class
num_: array size
mptr_: address of method
args_: Arguments of method
*/
template <typename Obj_ptr_array_t, typename Method_t, typename... Args_t>
inline void lumpdo(Obj_ptr_array_t ptr_, int num_, Method_t mptr_, Args_t... args_)
{
        for(int i = 0; i < num_; i++)
            (ptr_[i] ->* mptr_)(args_...);
}

#endif