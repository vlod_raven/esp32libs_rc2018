#ifndef SIMPLEPID_H_
#define SIMPLEPID_H_

#include "Arduino.h"

#if defined __cplusplus
extern "C" {
#endif

class PID
{
    private:
    float kp, ki, kd, proc, output, outmax, outmin,
            inmax, inmin, target, deviation[3];

    public:
    PID(float kp_, float ki_, float kd_);
    ~PID();
    void setgain(float kp_,float ki_,float kd_);
    void setoutlim(float max_, float min_);
    void setinlim(float max_, float min_);
    void settarget(float target_);
    void setproc(float proc_);
    float calc();
    void reset();

    inline float getkp(){
        return kp;
    }
    inline float getki(){
        return ki;
    }
    inline float getkd(){
        return kd;
    }
    inline float getproc(){
        return proc;
    }
    inline float getoutput(){
        return output;
    }
    inline float gettarget(){
        return target;
    }
};

#if defined __cplusplus
}
#endif

#endif