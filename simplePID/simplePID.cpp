#include "simplePID.h"

PID::PID(float kp_, float ki_, float kd_)
{
    setgain(kp_,ki_,kd_);
    setoutlim(1.0, 0);
    setinlim(10,0);
    reset();
}

PID::~PID()
{
}

void PID::setgain(float kp_, float ki_, float kd_)
{
    if(kp_ < 0)
        kp = 0;
    else
        kp = kp_;
    if(ki_ < 0)
        ki = 0;
    else
        ki = ki_;
    if(kd_ < 0)
        kd = 0;
    else
        kd = kd_;
}

void PID::setoutlim(float max_, float min_)
{
    outmax = max_;
    outmin = min_;
}

void PID::setinlim(float max_, float min_)
{
    inmax = max_;
    inmin = min_;
}

void PID::settarget(float target_)
{
    target = target_;
}

void PID::setproc(float proc_)
{
    if(proc_ > inmax)
        proc = inmax;
    else if(proc_ < inmin)
        proc = inmin;
    else
        proc = proc_;
}

float PID::calc()
{
    deviation[2] = deviation[1];
    deviation[1] = deviation[0];
    deviation[0] = target - proc;
    float dev = deviation[0] - deviation[1];
    output = output + ((kp * dev) + (ki * deviation[0]) 
            + (kd * (dev - (deviation[1] - deviation[2]))));
    if(output > outmax)
        output = outmax;
    else if(output < outmin)
        output = outmin;
    return output;
}

void PID::reset()
{
    proc = 0;
    output = 0;
    target = 0;
    deviation[3]={};
}