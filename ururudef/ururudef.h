#ifndef URURUDEF_H_
#define URURUDEF_H_
#include <FreeRTOS.h>

//User Settings------------------------------------
#define UM_MODE UM_BTS //UM_BTS: BluetoothSerial, UM_PS3: PS3, UM_NONE: NONE
#define UM_OTAE 0  //OTA  0:DISABLE, 1:ENABLE
#define UM_SSID "espOTA"    //set SSID for OTA
#define UM_WIFIPASS "roborobo"      //set password for wifi
#define UM_BTSNAME "Bauto" //set name for BTS
#define UM_DEBUGLVL 1
#define UM_DEBUGSERIAL Serial
//-------------------------------------------------

//PIN Assign -----------------------
//LED
#define UM_LED1_PIN GPIO_NUM_13
#define UM_LED2_PIN GPIO_NUM_23
#define UM_LED3_PIN GPIO_NUM_22
//SPI
#define UM_MOSI GPIO_NUM_32
#define UM_MISO GPIO_NUM_33
#define UM_SCLK GPIO_NUM_25
//SPI Slave select
#define UM_SS1 GPIO_NUM_5
#define UM_SS2 GPIO_NUM_17
#define UM_SS3 GPIO_NUM_16
#define UM_SS4 GPIO_NUM_4
#define UM_SS5 GPIO_NUM_0
#define UM_SS6 GPIO_NUM_2
#define UM_SS7 GPIO_NUM_15
//I2C
#define UM_SDA GPIO_NUM_26
#define UM_SCL GPIO_NUM_27
//Emergency Switch
#define UM_ESW GPIO_NUM_34
//----------------------------------

//Don't care -----------------------
#define UM_NONE 0
#define UM_PS3 1
#define UM_BTS 2

#if (UM_MODE == UM_BTS)
#define UM_SERIAL BTSerial
#else
#define UM_SERIAL Serial
#endif

#if defined __cplusplus
extern "C" {
#endif

typedef void (*fptr_vv_t)();

inline void wait(float time){
    vTaskDelay(time * 1000 / portTICK_RATE_MS);
}

#if defined __cplusplus
}
#endif

#define debug(x) (Serial.printf(#x" = %d\n",x))

#endif