#include <Arduino.h>
#include "ururudef.h"
#include "ururumother.h"
#include "mpu9250.h"
#include "denjiben.h"
#include "switch.h"
#include "MD_Position.h"
#include "Lightsensor.h"
//#include "ultrasonicsensor.h"
#include "TW_PID.h"
//#include "Omni_PID.h"

#undef UM_BTSNAME
#define UM_BTSNAME "Bauto"

#define KP  0.1
#define KI  0.01
#define KD  0.001

PID_MD md1_(&spi,UM_SS2,KP,KI,KD);
PID_MD md2_(&spi,UM_SS3,KP,KI,KD);
PID_MD_objptr_t md[2] = {&md1_, &md2_};
TW_PID tw(&md1_,&md2_);

denjiben sol1(&i2c);
Switch sw1(&i2c);
Lightsensor lsf(&i2c,LSADDRF);
Lightsensor lsb(&i2c,LSADDRB);
//USS uss(&i2c);

enum fieldcolor_t
{
    BLUE = 0,
    RED
};
fieldcolor_t fieldcolor = BLUE;
const char* getfc()
{
    static const char strB[] = "Blue";
    static const char strR[] = "Red";
    if(fieldcolor == BLUE)
        return strB;
    else
        return strR;
}

enum cylinder_t
{
    UNDEF = -1,
    RIGHT = 0,
    LEFT,
    FRONT,
    BACK,
    ANCHOR
};

enum autophase_t
{
    IDLE = 0,
    STARTDELAY,
    TOTOWER,
    ATTOWER,
    TURNATTOWER,
    TOBASE,
    ENDAUTO
};
autophase_t phase = IDLE;

float kp = 0.9, ki = 0.09, kd = 0.01, mp=1 ,target=1;
uint8_t remotestop = 0;
uint8_t remotectl = 0;
uint8_t mvtd[3] = {};
int cylorder[4] = {UNDEF,UNDEF,UNDEF,UNDEF};

//define for auto
//#define TURNANGLE -54.0
//#define TURNDURA 70
#define TURNDURA 80
//#define TOBASEDURA 200
#define TOBASEDURA 235
#define SOLDURA 800

void autotest2()
{
    static int ct1 = 0;
    static int ct2 = 0;
    static int flag = 0;
    /*if(remotestop == 1)
    {
        phase = IDLE;
        tw.brake();
        sol1.set(0b00000000);
    }*/
    if(readesw() == 1)
    {
        if(phase != IDLE)
        {
            ct1 = 0;
            ct2 = 0;
            flag = 0;
        }
        phase = IDLE;
        tw.brake();
        sol1.set(0b00000000);
    }
    //else if(remotectl == 1)
        //phase = IDLE;
    else if(readesw() == 0 && phase == IDLE)
    {
        tw.brake();
        phase = STARTDELAY;
        ct1 = 0;
        ct2 = 0;
        flag = 0;
    }
    switch(phase)
    {
        case IDLE:
        {
            static int ct0 = 0;
            static int flagc = 0;
            if((sw1.read() & 0b00001111) == 0b00000000)
            {
                for(int i = 0; i < 4; i++)
                    cylorder[i] = UNDEF;
                ct0 = 0;
            }
            else
            {
                if(ct0 < 4)
                {
                    for(int i = 0; i < 4; i++)
                    {
                        if(sw1.read(i))
                        {
                            for(int j = 0; j < ct0; j++)
                                if(cylorder[j] == i)
                                    flagc = 1;
                            if(flagc == 1)
                                flagc = 0;
                            else
                                cylorder[ct0++] = i;
                        }
                    }
                }
            }
            break;
        }
        case ENDAUTO: ct1 = 0; ct2 = 0; flag = 0; break;
        case STARTDELAY:
            ct1++;
            if(ct1 == 25)
            {
                phase = TOTOWER;
                ct1 = 0;
            }
            break;
        case TOTOWER:
        {
            if(sw1.read() & (0b1 << 4))
            {
                tw.brake();
                ct1++;
                if(ct1 > 30)
                {
                    phase = ATTOWER;
                    ct1 = 0;
                    ct2 = 0;
                    flag = 0;
                }
            }
            else if(flag == 2 && lsf.judge(middle) && lsb.judge(middle))
                tw.move(1.4);
            else if(flag == 2 && lsb.judge(middle) && lsf.judge(fieldcolor == BLUE ? right : left))
                tw.turn(fieldcolor == BLUE ? -1 : 1);
            else if(flag == 2 && lsb.judge(middle) && lsf.judge(fieldcolor == BLUE ? left : right))
                tw.turn(fieldcolor == BLUE ? 1 : -1);
            else if(lsb.judge(middle))
            {
                if(ct2 < 15)
                {
                    tw.brake();
                    ct2++;
                }
                else
                {
                    tw.turn(fieldcolor == BLUE ? 1.5 : -1.5);
                    flag = 2;
                }
            }
            else if(flag == 0 && lsf.judge(middle))
                flag = 1;
            else
            {
                if(flag == 0)
                    tw.move(2.8);
                else
                    tw.move(1.2);
            }
        }
        break;
        case ATTOWER:
        {
            ct1++;
            if(ct1 == 1)
            {
                sol1.set(ANCHOR,1);
                ct1 += 50;
            }
            else if(ct1 % 75 == 0)
            {
                if(ct2 == 4)
                {
                    sol1.set(ANCHOR,0);
                    ct2 = 5;
                    ct1 += 50;
                }
                else if(ct2 == 5)
                {
                    phase = TURNATTOWER;
                    ct1 = 0;
                    ct2 = 0;
                }
                else if(ct2 < 1 && (sw1.read(cylorder[0])==1) )
                {
                    sol1.settime(cylorder[0],SOLDURA);
                    ct2 = 1;
                }
                else if(ct2 < 2 && (sw1.read(cylorder[1])==1) )
                {
                    sol1.settime(cylorder[1],SOLDURA);
                    ct2 = 2;
                }
                else if(ct2 < 3 && (sw1.read(cylorder[2])==1) )
                {
                    sol1.settime(cylorder[2],SOLDURA);
                    ct2 = 3;
                }
                else if(ct2 < 4 && (sw1.read(cylorder[3])==1) )
                {
                    sol1.settime(cylorder[3],SOLDURA);
                    ct2 = 4;
                    ct1 += 50;
                }
                else
                {
                    ct2 = 4;
                    ct1 += 50;
                }
            }
        }
        break;
        case TURNATTOWER:
        {
            if(ct1 == 0)
            {
                tw.move(-1.5);
                ct1++;
            }
            else if(ct1 < 34)
                ct1++;
            else
            {
                if(ct2 == 0)
                {
                    tw.turn(fieldcolor == BLUE ? -1.5 : 1.5);
                    ct2++;
                }
                else if(ct2 == TURNDURA)
                {
                    tw.brake();
                    ct1 = 0;
                    ct2 = 0;
                    flag = 0;
                    phase = TOBASE;
                }
                else
                    ct2++;
                /*
                switch(tw.turnprog())
                {
                    case -1: tw.turnuntil(fieldcolor == BLUE ? TURNANGLE : -TURNANGLE); break;
                    case 0: break;
                    case 1: phase = TOBASE; ct1=0; break;
                }
                */
            }
        }
        break;
        case TOBASE:
        {
            if(ct1 == 0)
            {
                tw.move(-3);
                ct1++;
            }
            else if(ct1 < TOBASEDURA)
                ct1++;
            else if(ct1 == TOBASEDURA)
            {
                tw.brake();
                ct1 = 0;
                phase = ENDAUTO;
            }
        }
        break;
    }
}

void PID_tune()
{
    int c,serialflag=0;
    if(BTSerial.available())
    {
        static int ct = 0;
        c = BTSerial.read();
        if(c == 0x11)
            ct = 3;
        else if(ct > 0)
        {
            mvtd[3-ct] = c;
            ct--;
        }
        else
            serialflag = 1;
    }
    else if(Serial.available())
    {
        c = Serial.read();
        serialflag = 1;
    }
    if(serialflag == 1)
    {
        serialflag = 0;
        switch(c)
        {
            case '1': target += 1*mp; break;
            case '2': target -= 1*mp; break;
            case '3': kp += 1*mp; break;
            case '4': kp -= 1*mp; break;
            case '5': ki += 1*mp; break;
            case '6': ki -= 1*mp; break;
            case '7': kd += 1*mp; break;
            case '8': kd -= 1*mp; break;
            //case '9': mp *= 10; break;
            //case '0': mp /= 10; break;
            case 'p':
                tw.setgain(kp,ki,kd);
                tw.brake();
                break;
            case 'j': sol1.settime(LEFT,SOLDURA); break;
            case 'k': sol1.settime(BACK,SOLDURA); break;
            case 'l': sol1.settime(RIGHT,SOLDURA); break;
            case 'i': sol1.settime(FRONT,SOLDURA); break;
            case '[': fieldcolor = BLUE; break;
            case ']': fieldcolor = RED; break;
            case '.': remotestop = 0; break;
            case ',': remotestop = 1; break;
            case ';': remotectl = 0; break;
            case ':': remotectl = 1; break;
        }
        if(c == 'b') tw.brake();
        else if(c == 'w') tw.move(target);
        else if(c == 's') tw.move(-target);
        else if(c == 'a') tw.turn(target);
        else if(c == 'd') tw.turn(-target);
        else if(c == 'q') tw.turnuntil(90);
        else if(c == 'e') tw.turnuntil(-90);
        //else if(c == 'z') tw.moveuntil(2);
        //else if(c == 'c') tw.moveuntil(-2);
        else tw.move(0);
    }
    //UM_SERIAL.printf("i1:%.3f, i2:%.3f, o1:%.3f, o2:%.3f, t:%.3f, y:%.1f, lf:%1d%1d%1d, lb:%1d%1d%1d, sw:%d, esw:%d, FC:%s\n",md1_.getrps(), md2_.getrps(), md1_.getoutput(), md2_.getoutput(), target,mpu.getyaw(),lsf.judge(left),lsf.judge(middle),lsf.judge(right),lsb.judge(left),lsb.judge(middle),lsb.judge(right),sw1.read(),readesw(),getfc());
    //UM_SERIAL.printf("i1:%.3f, i2:%.3f, o1:%.3f, o2:%.3f, t:%.3f, y:%.1f, lf:%1d%1d%1d, lb:%1d%1d%1d, sw:%d, esw:%d, FC:%s, mvtd:%3d %3d %3d\n",md1_.getrps(), md2_.getrps(), md1_.getoutput(), md2_.getoutput(), target,mpu.getyaw(),lsf.judge(left),lsf.judge(middle),lsf.judge(right),lsb.judge(left),lsb.judge(middle),lsb.judge(right),sw1.read(),readesw(),getfc(),mvtd[0],mvtd[1],mvtd[2]);
    //if(BTSerial.hasClient)
        //UM_SERIAL.printf("i1:%.3f, i2:%.3f, o1:%.3f, o2:%.3f, t:%.3f, y:%.1f, lf:%1d%1d%1d, lb:%1d%1d%1d, sw:%d, esw:%d, FC:%s ord:%2d%2d%2d%2d\n",md1_.getrps(), md2_.getrps(), md1_.getoutput(), md2_.getoutput(), target,mpu.getyaw(),lsf.judge(left),lsf.judge(middle),lsf.judge(right),lsb.judge(left),lsb.judge(middle),lsb.judge(right),sw1.read(),readesw(),getfc(),cylorder[0],cylorder[1],cylorder[2],cylorder[3]);
}

void setup()
{
    UMtask_i2c_init = [](){     //i2cを使用するものの初期化はここに
        if(i2c.alive(MPU9250_ADDR))
        {
            Serial.printf("MPU9250 is alive\n");
            mpu9250_init();
        }
        else
            Serial.printf("MPU9250 is not alive\n");
        if(i2c.alive(DENJIADDR))
            Serial.printf("DENJIBEN is alive\n");
        else
            Serial.printf("DENJIBEN is not alive\n");
        if(i2c.alive(SWITCHADDR))
            Serial.printf("SWITCH is alive\n");
        else
            Serial.printf("SWITCH is not alive\n");
        if(i2c.alive(LSADDRF))
            Serial.printf("LSF is alive\n");
        else
            Serial.printf("LSF is not alive\n");
        if(i2c.alive(LSADDRB))
            Serial.printf("LSB is alive\n");
        else
            Serial.printf("LSB is not alive\n");
        /*if(i2c.alive(USSADDR))
            Serial.printf("USS is alive\n");
        else
            Serial.printf("USS is not alive\n");*/
    };

    UMtask_spi_init = [](){     //spiを使用するものの初期化はここに
    };

    UMtask_i2c_while = [](){    //10msごとに実行,i2cを使用するものはここに
        static int ct = 0;
        switch(ct)
        {
            case 0:
                sol1.update();
                sol1.com();
                sw1.com();
                break;
            case 1:
                //mpu9250_while();
                lsf.com();
                lsb.com();
                //uss.com();
                ct = -1;
                break;
        }
        ct++;
    };

    UMtask_spi_while = [](){    //10msごとに実行,spiを使用するものはここに
        static int ct = 0;
        switch(ct)
        {
            case 0:
                break;
            case 1:
                PID_tune();
                autotest2();
                PID_MD_while();
                ct = -1;
                break;
        }
        ct++;
    };

    PID_MD_setmds(md,2);    //モタドラクラスオブジェクトへのポインタの配列とその大きさ
    PID_MD_setfp1([](){     //モタドラとの通信前に実行
        tw.update();
    });

    md1_.md.reverse(1,1);
    md2_.md.reverse(0,0);    //モーター出力、エンコーダーを反転

    lsf.reverse(1);
    lsb.reverse(1);
    lsb.setthresh(middle,27);
    lsf.setthresh(middle,40);

    mpu.setreverse(1);

    UMinit();   //必須
    tw.brake();
}

void loop()
{
    #if (UM_OTAE == 1)
    ArduinoOTA.handle();
    #endif
    static int ct = 0;
    if(BTSerial.hasClient())
        BTSerial.printf("i1:%.3f, i2:%.3f, o1:%.3f, o2:%.3f, t:%.3f, y:%.1f, lf:%1d%1d%1d, lb:%1d%1d%1d, sw:%d, esw:%d, FC:%s ord:%2d%2d%2d%2d\n",md1_.getrps(), md2_.getrps(), md1_.getoutput(), md2_.getoutput(), target,mpu.getyaw(),lsf.judge(left),lsf.judge(middle),lsf.judge(right),lsb.judge(left),lsb.judge(middle),lsb.judge(right),sw1.read(),readesw(),getfc(),cylorder[0],cylorder[1],cylorder[2],cylorder[3]);
    if(ct % 5 == 0) 
        Serial.printf("$%d\n",ct/5);
    ct++;
    wait(0.2);
}