#ifndef SIMPLE_PID_MD_H_
#define SIMPLE_PID_MD_H_

//#include "Arduino.h"
#include "MD.h"
#include "simplePID.h"
#include "ururudef.h"

//#define DUTYMODE
#define INMAX 10
#define INMIN -10
#define OUTMAX 1.0
#define OUTMIN -1.0

#if defined __cplusplus
extern "C"{
#endif

class PID_MD
{
    public:
    MSPI *spi;
    MD md;

    float rps,output;

    enum status{
        IDLE = 0,
        BRAKE = 1,
        FRONT = 2,
        BACK = 3
    };
    enum status stat = IDLE;

    public:
    PID pid;
    PID_MD(MSPI *spi_, gpio_num_t ss_, float kp_, float ki_, float kd_);
    void setgain(float,float,float);
    void settarget(float);
    void setproc();
    void calc();
    void com();
    void com_rps();
    void reset();
    void brake();

    inline status getstatus(){
        return stat;
    }
    inline float getrps(){
        return rps;
    }
    inline float getoutput(){
        return output;
    }
    inline float gettarget()
    {
        return pid.gettarget();
    }

};

typedef PID_MD (*PID_MD_objptr_t);
void PID_MD_setmds(PID_MD_objptr_t ptr_[], int num_);
void PID_MD_setfp1(fptr_vv_t fp_);
void PID_MD_while();

#if defined __cplusplus
}
#endif
#endif