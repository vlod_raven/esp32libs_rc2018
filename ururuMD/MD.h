#ifndef __MD_H__
#define __MD_H__

#include "ururudef.h"
#include <Arduino.h>
#include "Mbed.h"


#define ENCODER_RES 400
//#define ENCODER_RES 918
//#define ENCODER_RES 3672

#if defined __cplusplus
extern "C"{
#endif

class MD{
    public:
    MSPI *spi;
    uint16_t Send16(uint16_t data);
    uint16_t SetDuty(float duty);
    double operator = (float duty); 
    void init(MSPI *spi_, gpio_num_t cs);     
    MD(MSPI *spi_, gpio_num_t cs);  
    int setduty(float duty);
    double setduty_rps(float duty);
    void reverse(int mdrv,int qeirv);
    void setres(int res_);
    
    inline float getrev()
    {
        return (float)pulsesum / (float)res;
    }

    inline float getpulsesum()
    {
        return pulsesum;
    }

    private:
    unsigned long prev_time = 0;
    DigitalOut CS;
    uint8_t temperature;
    int pulsesum;
    int count_tmp;
    int mdrv,qeirv,res;
};

#if defined __cplusplus
}
#endif
#endif