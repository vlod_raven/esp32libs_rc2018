#include "simplePID_MD.h"
#include "../ravenlib/lumpdo.h"
#include "ururudef.h"
#include "Mbed.h"
#ifdef DEBUG_PID_MD
#endif

PID_MD::PID_MD(MSPI *spi_, gpio_num_t ss_, float kp_, float ki_, float kd_)
:pid(kp_,ki_,kd_)
,md(spi_,ss_)
{
    #ifndef DUTYMODE
    pid.setinlim(INMAX,INMIN);
    #else
    pid.setinlim(OUTMAX,OUTMIN);
    #endif
    pid.setoutlim(OUTMAX,OUTMIN);
    reset();
}

void PID_MD::setgain(float kp_, float ki_, float kd_)
{
    pid.setgain(kp_,ki_,kd_);
}

void PID_MD::settarget(float target_)
{
    stat = IDLE;
    pid.settarget(target_);
}

void PID_MD::setproc()
{
    #ifndef DUTYMODE
    pid.setproc(rps);
    #else
    pid.setproc(output);
    #endif
}

void PID_MD::calc()
{
    if(stat == BRAKE)
        output = 0;
    else
        output = pid.calc();
}

void PID_MD::com()
{
    md.setduty(output);
}

void PID_MD::com_rps()
{
    float tmp;
    tmp = md.setduty_rps(output);
    if(abs(tmp) < abs(INMAX))
        rps = tmp;
}

void PID_MD::reset()
{
    pid.reset();
    rps = 0;
    output = 0;
}

void PID_MD::brake()
{
    stat = BRAKE;
    settarget(0);
    reset();
}

PID_MD_objptr_t *PID_MD_objptr = NULL;
int PID_MD_num = 0;
fptr_vv_t PID_MD_fp1 = [](){};

void PID_MD_setmds(PID_MD_objptr_t ptr_[], int num_)
{
    PID_MD_objptr = ptr_;
    PID_MD_num = num_;
}

void PID_MD_setfp1(fptr_vv_t fp_)
{
    PID_MD_fp1 = fp_;
}

void PID_MD_while()
{
    PID_MD_fp1();
    lumpdo(PID_MD_objptr,PID_MD_num,&PID_MD::com_rps); //update rps
    lumpdo(PID_MD_objptr,PID_MD_num,&PID_MD::setproc); //set rps process
    lumpdo(PID_MD_objptr,PID_MD_num,&PID_MD::calc); //calculate
    lumpdo(PID_MD_objptr,PID_MD_num,&PID_MD::com); //send new output
}
