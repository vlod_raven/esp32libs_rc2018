//MD v1.0 2018/6/15
#include "MD.h"

uint16_t MD::Send16(uint16_t data){
    union bit16{
        uint16_t b16;
        uint8_t byte[2];
    };

    bit16 d;
    d.b16 = data;
    uint16_t receive = 0x0000;

    if(d.byte[0] == 230)
        d.byte[0]++;

    //First transmission    
    CS = 0;
    temperature = spi->write(d.byte[0]);   //send PWM3DCH    receive dummy[temp]
    CS = 1;
    wait(0.0001);

    //Second transmission
    CS = 0;
    receive = spi->write(d.byte[1]);//send PWM3DCL    receive CountH
    CS = 1;
    receive = receive << 8;
    wait(0.0001);

    //Third transmission    
    CS = 0;
    receive = receive | spi->write(193);//send dummy      receive CountL
    CS = 1;
    wait(0.0001);

    return receive;
}

uint16_t MD::SetDuty(float duty){
    if(duty > 1.0)
        return Send16(973);
    if(duty < -1.0)
        return Send16(52);

    duty *= 0.90;
    int val = duty * 512 + 512;
    return Send16(val);
}

double MD::operator = (float duty){
    unsigned long now_time = micros();
    double dif_time = (double)(now_time - prev_time) / 1000000.0;// us -> s

    int count = SetDuty(duty) - 32768;
    
    //pulsesum += count;
    double rps = ( (double)count / dif_time / 400.0 );//100pulse
    prev_time = now_time;
    return rps*(-1.0);   
}

int MD::setduty(float duty){
    if(mdrv != 0)
        duty = -duty;
    int count = SetDuty(duty) - 32768;
    if(qeirv != 0)
        count = -count;
    //pulsesum += count;
    count_tmp += count;
    return count;   
}

double MD::setduty_rps(float duty){
    unsigned long now_time = micros();
    double dif_time = (double)(now_time - prev_time) / 1000000.0;// us -> s

    if(mdrv != 0)
        duty = -duty;
    int count = SetDuty(duty) - 32768;
    if(qeirv != 0)
        count = -count;
    //pulsesum += count;
    float rps = (((double)(count + count_tmp) / res) / dif_time);
    prev_time = now_time;
    count_tmp = 0;
    return rps;   
}

void MD::init(MSPI *spi_, gpio_num_t cs){
    spi = spi_;    
    CS.init(cs);
    CS = 1;
    count_tmp = 0;
}

MD::MD(MSPI *spi_, gpio_num_t cs){
    init(spi_,cs);
    prev_time = micros();
    mdrv = 0;
    qeirv = 0;
    res = ENCODER_RES;
    pulsesum = 0;
}

void MD::reverse(int mdrv_, int qeirv_)
{
    mdrv = mdrv_;
    qeirv = qeirv_;
}

void MD::setres(int res_)
{
    res = res_;
}