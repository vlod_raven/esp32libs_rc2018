#ifndef MPU9250_H_
#define MPU9250_H_

#include <Arduino.h>
#include "ururumother.h"
#include "MPU9250_asukiaaa.h"

#define MPU9250_ADDR 0x68

#if defined __cplusplus
extern "C" {
#endif

class mpu9250
{
    private:
    int mpu_err;
    int err_ct=0;
    float gz,yaw;
    float ogz;
    unsigned long prev_time;
    uint8_t reverse;

    public:
    MPU9250 mpu;
    mpu9250(TwoWire*);
    void setorigin();
    int update();
    void angleupdate();
    float getyaw();
    void setreverse(uint8_t rev_);
};

extern mpu9250 mpu;

void mpu9250_init();
void mpu9250_while();

#if defined __cplusplus
}
#endif

#endif