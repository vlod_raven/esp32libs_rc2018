#include "mpu9250.h"
#include "ururudef.h"

#define MPURETRYCT 10
#define MPUDEADZONE 1.0

mpu9250::mpu9250(TwoWire *wire_)
{
    mpu.setWire(wire_);
    mpu.beginGyro(GYRO_FULL_SCALE_500_DPS);
    reverse = 0;
}

void mpu9250::setorigin()
{
    mpu.gyroZUpdate();
    ogz = mpu.gyroZ();
    for(int i = 0; i < 5; i++)
    {
        mpu.gyroZUpdate();
        ogz = (ogz + mpu.gyroZ()) / 2;
        vTaskDelay(1 / portTICK_RATE_MS);
    }
    yaw = 0;
}

int mpu9250::update()
{
    if((mpu_err = mpu.gyroZUpdate()) == 0)
    {
        if(err_ct > 10)
        {
            wait(1.0);
            setorigin();
        }
        gz = mpu.gyroZ() - ogz;
        if(gz > -MPUDEADZONE && gz < MPUDEADZONE)
            gz = 0;
        angleupdate();
        err_ct = 0;
        return 0;
    }
    else
    {
        gz = 0;
        angleupdate();
        err_ct++;
        if(err_ct >= 10)
            if((err_ct % MPURETRYCT) == 0)
                mpu.beginGyro(GYRO_FULL_SCALE_500_DPS);
        return mpu_err;
    }
}

void mpu9250::angleupdate()
{
    unsigned long now_time = micros(); 
    yaw = yaw + gz * (now_time - prev_time) / 1000000;
    prev_time = now_time;
}

float mpu9250::getyaw()
{
    if(reverse)
        return -yaw/2;
    return yaw/2;
}

//==========================================

mpu9250 mpu = mpu9250(&Wire);
int mpu9250_alive = 0;

void mpu9250_init()
{
    if((mpu9250_alive = i2c.alive(MPU9250_ADDR)) == 1)
    {
        mpu.setorigin();
    }
    else
    { 
    }
}

void mpu9250_while()
{
    if(mpu9250_alive == 1)
    {
        #if (UM_DEBUGLVL >= 1)
        int err;
        if((err = mpu.update()) != 0)
            UM_DEBUGSERIAL.printf("mpu: err = %d\n",err);
        #else
        mpu.update();
        #endif
    }
}

void mpu9250::setreverse(uint8_t rev_)
{
    reverse = rev_;
}