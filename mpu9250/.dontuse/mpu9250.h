#ifndef MPU9250_H_
#define MPU9250_H_

#include "ururumother.h"
#include "MPU9250_asukiaaa.h"
#include "MadgwickAHRS.h"

#if defined __cplusplus
extern "C" {
#endif

namespace mpu9250
{
    //extern MPU9250 mpu;
    //extern Madgwick mw;
    //extern float gx,gy,gz,ax,ay,az;
    //extern float ogx,ogy,ogz,oax,oay,oaz;
    
    void init();
    void setorigin();
    void update();
    void printimu();
    void printangle();

    void initgz();
    void setorigingz();
    void updategz();
    void printgz();
    void printanglegz();
};

#if defined __cplusplus
}
#endif

#endif