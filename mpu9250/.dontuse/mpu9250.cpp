#include "mpu9250.h"

namespace mpu9250
{

MPU9250 mpu;
Madgwick mw;
float gx,gy,gz,ax,ay,az,mx,my,mz,roll,pitch,yaw;
float ogx,ogy,ogz,oax,oay,oaz,omx,omy,omz;

void init()
{
    mpu.setWire(&Wire);
    mpu.beginAccel(ACC_FULL_SCALE_4_G);
    mpu.beginGyro(GYRO_FULL_SCALE_500_DPS);
    mpu.beginMag();
    mw.begin(50);
}

void setorigin()
{
    mpu.accelUpdate();
    oax = mpu.accelX();
    oay = mpu.accelY();
    oaz = mpu.accelZ();
    mpu.gyroUpdate();
    ogx = mpu.gyroX();
    ogy = mpu.gyroY();
    ogz = mpu.gyroZ();
    mpu.magUpdate();
    omx = mpu.magX();
    omy = mpu.magY();
    omz = mpu.magZ();
    for(int i = 0; i < 10; i++)
    {
        mpu.accelUpdate();
        oax = (oax + mpu.accelX()) / 2;
        oay = (oay + mpu.accelY()) / 2;
        oaz = (oaz + mpu.accelZ()) / 2;
        mpu.gyroUpdate();
        ogx = (ogx + mpu.gyroX()) / 2;
        ogy = (ogy + mpu.gyroY()) / 2;
        ogz = (ogz + mpu.gyroZ()) / 2;
        mpu.magUpdate();
        omx = (omx + mpu.magX()) / 2;
        omy = (omy + mpu.magY()) / 2;
        omz = (omz + mpu.magZ()) / 2;
    }
    wait(0.001);
}

void update()
{
    mpu.accelUpdate();
    ax = mpu.accelX() - oax;
    ay = mpu.accelY() - oay;
    az = mpu.accelZ() - oaz;
    mpu.gyroUpdate();
    gx = mpu.gyroX() - ogx;
    gy = mpu.gyroY() - ogy;
    gz = mpu.gyroZ() - ogz;
    mpu.magUpdate();
    mx = mpu.magX();
    my = mpu.magY();
    mz = mpu.magZ();

    mw.update(gx,gy,gz,ax,ay,az,mx,my,mz);
    roll = mw.getRoll();
    pitch = mw.getPitch();
    yaw = mw.getYaw();
}

void printimu()
{
    Serial.println("accelX: " + String(ax));
    Serial.println("accelY: " + String(ay));
    Serial.println("accelZ: " + String(az));
    Serial.println("gyroX: " + String(gx));
    Serial.println("gyroY: " + String(gy));
    Serial.println("gyroZ: " + String(gz));
}

void printangle()
{
    Serial.printf("R: %5f | P: %5f | Y: %5f \n",roll,pitch,yaw);
}

}